# Use bundled deps as we don't ship the exact right versions for all the
# required rust libraries
%global bundled_rust_deps 1

Name:           gnome-tour
Version:        44.0
Release:        1
Summary:        GNOME Tour and Greeter
License:        GPLv3+
URL:            https://gitlab.gnome.org/GNOME/gnome-tour
Source0:        https://download.gnome.org/sources/%{name}/44/%{name}-%{version}.tar.xz

BuildRequires:  meson git
BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(gio-2.0)
BuildRequires:  pkgconfig(gdk-pixbuf-2.0)
BuildRequires:  pkgconfig(gtk4)
BuildRequires:  pkgconfig(libadwaita-1)
BuildRequires:  /usr/bin/appstream-util
BuildRequires:  /usr/bin/desktop-file-validate

%if 0%{?bundled_rust_deps}
BuildRequires:  cargo
BuildRequires:  rust
%else
BuildRequires:  rust-packaging
%endif

%if 0%{?bundled_rust_deps}
# bundled crates list updated for gnome-tour 42.beta
Provides: bundled(crate(aho-corasick/default)) = 0.7.18
Provides: bundled(crate(anyhow/default)) = 1.0.53
Provides: bundled(crate(atty/default)) = 0.2.14
Provides: bundled(crate(autocfg/default)) = 1.0.1
Provides: bundled(crate(bitflags/default)) = 1.3.2
Provides: bundled(crate(block/default)) = 0.1.6
Provides: bundled(crate(cairo-rs/default)) = 0.15.1
Provides: bundled(crate(cairo-sys-rs/default)) = 0.15.1
Provides: bundled(crate(cc/default)) = 1.0.72
Provides: bundled(crate(cfg-expr/default)) = 0.9.0
Provides: bundled(crate(cfg-if/default)) = 1.0.0
Provides: bundled(crate(env_logger/default)) = 0.7.1
Provides: bundled(crate(field-offset/default)) = 0.3.4
Provides: bundled(crate(futures-channel/default)) = 0.3.19
Provides: bundled(crate(futures-core/default)) = 0.3.19
Provides: bundled(crate(futures-executor/default)) = 0.3.19
Provides: bundled(crate(futures-io/default)) = 0.3.19
Provides: bundled(crate(futures-task/default)) = 0.3.19
Provides: bundled(crate(futures-util/default)) = 0.3.19
Provides: bundled(crate(gdk4/default)) = 0.4.4
Provides: bundled(crate(gdk-pixbuf/default)) = 0.15.1
Provides: bundled(crate(gdk-pixbuf-sys/default)) = 0.15.1
Provides: bundled(crate(gdk4-sys/default)) = 0.4.2
Provides: bundled(crate(gettext-rs/default)) = 0.7.0
Provides: bundled(crate(gettext-sys/default)) = 0.21.2
Provides: bundled(crate(gio/default)) = 0.15.3
Provides: bundled(crate(gio-sys/default)) = 0.15.1
Provides: bundled(crate(glib/default)) = 0.15.3
Provides: bundled(crate(glib-macros/default)) = 0.15.3
Provides: bundled(crate(glib-sys/default)) = 0.15.1
Provides: bundled(crate(gobject-sys/default)) = 0.15.1
Provides: bundled(crate(graphene-rs/default)) = 0.15.1
Provides: bundled(crate(graphene-sys/default)) = 0.15.1
Provides: bundled(crate(gsk4/default)) = 0.4.4
Provides: bundled(crate(gsk4-sys/default)) = 0.4.2
Provides: bundled(crate(gtk4/default)) = 0.4.5
Provides: bundled(crate(gtk4-macros/default)) = 0.4.3
Provides: bundled(crate(gtk4-sys/default)) = 0.4.5
Provides: bundled(crate(heck/default)) = 0.3.3
Provides: bundled(crate(heck/default)) = 0.4.0
Provides: bundled(crate(humantime/default)) = 1.3.0
Provides: bundled(crate(lazy_static/default)) = 1.4.0
Provides: bundled(crate(libc/default)) = 0.2.74
Provides: bundled(crate(libadwaita/default)) = 0.1.0
Provides: bundled(crate(libadwaita-sys/default)) = 0.1.0
Provides: bundled(crate(libc/default)) = 0.2.114
Provides: bundled(crate(locale_config/default)) = 0.3.0
Provides: bundled(crate(log/default)) = 0.4.14
Provides: bundled(crate(malloc_buf/default)) = 0.0.6
Provides: bundled(crate(memchr/default)) = 2.4.1
Provides: bundled(crate(memoffset/default)) = 0.6.5
Provides: bundled(crate(objc/default)) = 0.2.7
Provides: bundled(crate(objc-foundation/default)) = 0.1.1
Provides: bundled(crate(objc_id/default)) = 0.1.1
Provides: bundled(crate(pango/default)) = 0.15.2
Provides: bundled(crate(pango-sys/default)) = 0.15.1
Provides: bundled(crate(pest/default)) = 2.1.3
Provides: bundled(crate(pin-project-lite/default)) = 0.2.8
Provides: bundled(crate(pin-utils/default)) = 0.1.0
Provides: bundled(crate(pkg-config/default)) = 0.3.24
Provides: bundled(crate(pretty_env_logger/default)) = 0.4.0
Provides: bundled(crate(proc-macro-crate/default)) = 1.1.0
Provides: bundled(crate(proc-macro-error/default)) = 1.0.4
Provides: bundled(crate(proc-macro-error-attr/default)) = 1.0.4
Provides: bundled(crate(proc-macro2/default)) = 1.0.36
Provides: bundled(crate(quick-error/default)) = 1.2.3
Provides: bundled(crate(quote/default)) = 1.0.15
Provides: bundled(crate(regex/default)) = 1.5.4
Provides: bundled(crate(regex-syntax/default)) = 0.6.25
Provides: bundled(crate(rustc_version/default)) = 0.3.3
Provides: bundled(crate(semver/default)) = 0.11.0
Provides: bundled(crate(semver-parser/default)) = 0.10.2
Provides: bundled(crate(serde/default)) = 1.0.136
Provides: bundled(crate(slab/default)) = 0.4.5
Provides: bundled(crate(smallvec/default)) = 1.8.0
Provides: bundled(crate(syn/default)) = 1.0.86
Provides: bundled(crate(system-deps/default)) = 6.0.0
Provides: bundled(crate(temp-dir/default)) = 0.1.11
Provides: bundled(crate(termcolor/default)) = 1.1.2
Provides: bundled(crate(thiserror/default)) = 1.0.30
Provides: bundled(crate(thiserror-impl/default)) = 1.0.30
Provides: bundled(crate(toml/default)) = 0.5.8
Provides: bundled(crate(ucd-trie/default)) = 0.1.3
Provides: bundled(crate(unicode-segmentation/default)) = 1.8.0
Provides: bundled(crate(unicode-xid/default)) = 0.2.2
Provides: bundled(crate(version-compare/default)) = 0.1.0
Provides: bundled(crate(version_check/default)) = 0.9.4
%endif

# Removed in F34
Obsoletes: gnome-getting-started-docs < 3.38.1-2
Obsoletes: gnome-getting-started-docs-cs < 3.38.1-2
Obsoletes: gnome-getting-started-docs-de < 3.38.1-2
Obsoletes: gnome-getting-started-docs-es < 3.38.1-2
Obsoletes: gnome-getting-started-docs-fr < 3.38.1-2
Obsoletes: gnome-getting-started-docs-gl < 3.38.1-2
Obsoletes: gnome-getting-started-docs-hu < 3.38.1-2
Obsoletes: gnome-getting-started-docs-it < 3.38.1-2
Obsoletes: gnome-getting-started-docs-pl < 3.38.1-2
Obsoletes: gnome-getting-started-docs-pt_BR < 3.38.1-2
Obsoletes: gnome-getting-started-docs-ru < 3.38.1-2

%description
A guided tour and greeter for GNOME.

%prep
%autosetup -p1 -n %{name}-%{version}

%if ! 0%{?bundled_rust_deps}
sed -i -e '/\(build_by_default\|install\)/s/true/false/' src/meson.build
%cargo_prep
%endif

%if ! 0%{?bundled_rust_deps}
%generate_buildrequires
%cargo_generate_buildrequires
%endif

%build
%meson -Dprofile=development
%meson_build

%if ! 0%{?bundled_rust_deps}
%cargo_build
%endif

%install
%meson_install
mv $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/scalable/apps/org.gnome.TourDevel.svg $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/scalable/apps/org.gnome.Tour.svg
mv $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/symbolic/apps/org.gnome.TourDevel-symbolic.svg $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/symbolic/apps/org.gnome.Tour-symbolic.svg
mv $RPM_BUILD_ROOT%{_metainfodir}/org.gnome.TourDevel.metainfo.xml $RPM_BUILD_ROOT%{_metainfodir}/org.gnome.Tour.metainfo.xml
mv $RPM_BUILD_ROOT%{_datadir}/applications/org.gnome.TourDevel.desktop $RPM_BUILD_ROOT%{_datadir}/applications/org.gnome.Tour.desktop
sed -i 's/TourDevel/Tour/g' $RPM_BUILD_ROOT%{_metainfodir}/org.gnome.Tour.metainfo.xml
sed -i 's/TourDevel/Tour/g' $RPM_BUILD_ROOT%{_datadir}/applications/org.gnome.Tour.desktop

%if ! 0%{?bundled_rust_deps}
%cargo_install
%endif

%find_lang gnome-tour

%check
appstream-util validate-relax --nonet $RPM_BUILD_ROOT%{_metainfodir}/org.gnome.Tour.metainfo.xml
desktop-file-validate $RPM_BUILD_ROOT%{_datadir}/applications/org.gnome.Tour.desktop

%files -f gnome-tour.lang
%license LICENSE.md
%doc NEWS README.md
%{_bindir}/gnome-tour
%{_datadir}/gnome-tour/
%{_datadir}/applications/org.gnome.Tour.desktop
%{_datadir}/icons/hicolor/scalable/apps/org.gnome.Tour.svg
%{_datadir}/icons/hicolor/symbolic/apps/org.gnome.Tour-symbolic.svg
%{_metainfodir}/org.gnome.Tour.metainfo.xml

%changelog
* Thu Nov 23 2023 zhangxianting <zhangxianting@uniontech.com> - 44.0-1
- update to version 44.0

* Mon Mar 13 2023 lin zhang <lin.zhang@turbolinux.com.cn> - 43.0-2
- Enable debuginfo for fix strip

* Mon Jan 02 2023 lin zhang <lin.zhang@turbolinux.com.cn> - 43.0-1
- Update to 43.0

* Mon Jun 20 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 42.0-2
- Add gnome-tour.yaml

* Mon Mar 28 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 42.0-1
- Upgrade to 42.0

* Wed Jun 30 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 3.38.0-1
- Package init with 3.38.0
